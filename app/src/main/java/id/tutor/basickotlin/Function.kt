package id.tutor.basickotlin

fun main(){

    // Call Function

    makeCoffee(1,"Jimmy")

    makeCoffee(2,"Lady")

    makeCoffee(30,"Cepu")

}



// Define Function

fun makeCoffee(sugarCount : Int,name: String){
    var spoon = "";
    if(sugarCount == 1){
        println("Coffee with $sugarCount spoon of sugar for $name")
    }else{
        println("Coffee with $sugarCount spoons of sugar for $name")
    }




}


