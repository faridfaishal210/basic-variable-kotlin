package id.tutor.basickotlin

fun main() {
    var circleRatio = 3.14;
    println(circleRatio)
    circleRatio = 3.141592653589793;
    println(circleRatio)
}